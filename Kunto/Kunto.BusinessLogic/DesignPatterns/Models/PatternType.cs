namespace Kunto.BusinessLogic.DesignPatterns.Models
{
    public enum PatternType
    {
        Creational,
        Structural,
        Behavioral
    }
}