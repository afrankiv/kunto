﻿namespace Kunto.BusinessLogic.DesignPatterns.Models
{
    public class Pattern
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public PatternType PatternType { get; set; }
    }
}