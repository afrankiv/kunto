﻿using System.Collections.Generic;
using System.Web.Http;
using Kunto.BusinessLogic.DesignPatterns.Models;

namespace Kunto.Web.Services
{
    public class DesignPatternsController : ApiController
    {
        // GET api/designpatterns
        public IEnumerable<Pattern> Get()
        {
            return new List<Pattern>
            {
                new Pattern {Id = 1, Name = "Abstract Factory", PatternType = PatternType.Creational},
                new Pattern {Id = 2, Name = "Builder", PatternType = PatternType.Creational},
                new Pattern {Id = 3, Name = "Factory Method", PatternType = PatternType.Creational},
                new Pattern {Id = 4, Name = "Prototype", PatternType = PatternType.Creational},
                new Pattern {Id = 5, Name = "Singleton", PatternType = PatternType.Creational},

                new Pattern {Id = 6, Name = "Adapter", PatternType = PatternType.Structural},
                new Pattern {Id = 7, Name = "Bridge", PatternType = PatternType.Structural},
                new Pattern {Id = 8, Name = "Composite", PatternType = PatternType.Structural},
                new Pattern {Id = 9, Name = "Decorator", PatternType = PatternType.Structural},
                new Pattern {Id = 10, Name = "Facade", PatternType = PatternType.Structural},
                new Pattern {Id = 11, Name = "Flyweight", PatternType = PatternType.Structural},
                new Pattern {Id = 12, Name = "Proxy", PatternType = PatternType.Structural},

                new Pattern {Id = 13, Name = "Chain of Responsibility", PatternType = PatternType.Behavioral},
                new Pattern {Id = 14, Name = "Command", PatternType = PatternType.Behavioral},
                new Pattern {Id = 15, Name = "Interpreter", PatternType = PatternType.Behavioral},
                new Pattern {Id = 16, Name = "Iterator", PatternType = PatternType.Behavioral},
                new Pattern {Id = 17, Name = "Mediator", PatternType = PatternType.Behavioral},
                new Pattern {Id = 18, Name = "Memento", PatternType = PatternType.Behavioral},
                new Pattern {Id = 19, Name = "Observer", PatternType = PatternType.Behavioral},
                new Pattern {Id = 20, Name = "State", PatternType = PatternType.Behavioral},
                new Pattern {Id = 21, Name = "Strategy", PatternType = PatternType.Behavioral},
                new Pattern {Id = 22, Name = "Template Method", PatternType = PatternType.Behavioral},
                new Pattern {Id = 23, Name = "Visitor", PatternType = PatternType.Behavioral},
            };
        }

        // GET api/designpatterns/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/designpatterns
        public void Post([FromBody] string value)
        {
        }

        // PUT api/designpatterns/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/designpatterns/5
        public void Delete(int id)
        {
        }
    }
}