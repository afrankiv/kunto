﻿(function (designPatternsApp, baseApiUrl) {
    designPatternsApp.factory('designPatternsService', [
        '$http',
        '$q',
        function ($http, $q) {

            // ReSharper disable once InconsistentNaming
            function _getPatternsList() {
                var deferred = $q.defer();

                $http.get(baseApiUrl + 'DesignPatterns').success(function (data) {
                    deferred.resolve(data);
                });

                return deferred.promise;
            }

            return {
                getPatternsList: _getPatternsList
            }
        }]);
})(designPatternsApp, baseApiUrl);