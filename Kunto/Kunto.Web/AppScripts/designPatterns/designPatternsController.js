﻿(function (designPatternsApp) {
    designPatternsApp.controller('designPatternsController', [
        '$scope',
        'designPatternsService',
        function ($scope, designPatternsService) {

            $scope.patterns = [];

            designPatternsService.getPatternsList().then(function (data) {
                $scope.patterns = data;
            });
        }]);
})(designPatternsApp);