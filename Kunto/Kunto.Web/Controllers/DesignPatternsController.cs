﻿using System.Web.Mvc;

namespace Kunto.Web.Controllers
{
    public class DesignPatternsController : Controller
    {
        public ActionResult Index()
        {
            return View("Patterns");
        }
    }
}